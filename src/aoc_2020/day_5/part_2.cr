require "../puzzle"
require "./common"

module AoC2020::Day5
  class Part2
    include Puzzle
    include Common

    protected def solve
      input.each_line { |line| Seat.parse(line) }

      (0..127).each do |row|
        (0..7).each do |column|
          next if Seat.taken?(row, column)
          candidate = Seat.new(row, column)
          next unless Seat.taken?(candidate.id - 1)
          next unless Seat.taken?(candidate.id + 1)
          return output.puts(candidate.id)
        end
      end
    end
  end
end
