module AoC2020
  module Day3
    module Common
      struct Map
        def initialize(@rows : Array(Array(Bool)))
        end

        private def width
          @rows[0].size
        end

        private def height
          @rows.size
        end

        def [](x, y)
          row = @rows[y]
          row[x % width]
        end

        def trajectory(slope_x, slope_y)
          x, y = {0, 0}

          while y < height
            yield self[x, y]
            x += slope_x
            y += slope_y
          end
        end

        def self.parse(io : IO) : Map
          rows = Array(Array(Bool)).new
          io.each_line do |line|
            rows << Array(Bool).new

            line.each_char do |char|
              rows.last << (char == '#')
            end
          end

          new(rows)
        end
      end

      def tree_count_for_slope(map, x, y)
        tree_count = 0

        map.trajectory(x, y) do |tree|
          tree_count += 1 if tree
        end

        tree_count
      end
    end
  end
end
