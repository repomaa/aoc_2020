require "../puzzle"
require "./common"

module AoC2020::Day7
  class Part2
    include Puzzle
    include Common

    protected def solve
      Rule.parse_all(input)
      output.puts(Rule["shiny gold"].bag_count - 1)
    end
  end
end
