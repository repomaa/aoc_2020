require "../puzzle"
require "./common"

module AoC2020::Day4
  class Part2
    include Puzzle
    include Common

    protected def solve
      valid_count = 0
      each_passport do |passport|
        valid_count += 1 if passport.valid?
      end

      output.puts(valid_count)
    end
  end
end
