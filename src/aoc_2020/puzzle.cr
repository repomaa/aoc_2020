require "benchmark"
require "./client"

module AoC2020
  module Puzzle
    protected getter input : IO
    protected getter output : IO

    macro included
      def self.run(output : IO)
        slurped_input = IO::Memory.new

        {% day = @type.class.name.split("::")[-2].gsub(/.+(\d+)$/, "\\1") %}
        Client.fetch_input({{ day.id }}) do |input|
          IO.copy(input, slurped_input)
        end

        slurped_input.rewind
        cached_output = IO::Memory.new

        puzzle = new(slurped_input, cached_output)


        Benchmark.bm do |x|
          x.report(self.name) do
            puzzle.solve
          end
        end

        cached_output.rewind
        IO.copy(cached_output, output)
      end

      run(STDOUT)
    end

    protected def initialize(@input, @output)
    end

    protected abstract def solve
  end
end
