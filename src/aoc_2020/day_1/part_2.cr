require "../puzzle"
require "./common"

module AoC2020::Day1
  class Part2
    include Puzzle
    include Common

    protected def solve
      output.puts(better_solve(3))
    end
  end
end
