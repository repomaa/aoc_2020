require "colorize"

module AoC2020::Day7
  module Common
    class Rule
      @@rules = Hash(String, self).new

      getter color : String
      @bag_count : Int32?

      def initialize(@color, @containing : Array({Int32, String}))
        @containing_colors = Set(String).new(containing.each.map(&.[] 1))
        @positive_cache = Set(String).new(@containing_colors)
        @negative_cache = Set(String).new
      end

      def path_to(color)
        return nil unless can_contain?(color)
        return [self.color, color] if @containing_colors.includes?(color)
        [self.color] + each_containing.find { |_, rule| rule.can_contain?(color) }.not_nil![1].path_to(color).not_nil!
      end

      def can_contain?(color)
        return false if @negative_cache.includes?(color)
        return true if @positive_cache.includes?(color)

        each_containing do |_, rule|
          next unless rule.can_contain?(color)
          @positive_cache << color
          return true
        end

        @negative_cache << color
        false
      end

      def bag_count
        @bag_count ||= each_containing.reduce(1) do |sum, (quantity, rule)|
          sum + quantity * rule.bag_count
        end
      end

      def each_containing
        @containing.each.each do |(quantity, color)|
          yield quantity, @@rules[color]
        end
      end

      def each_containing
        @containing.each.map { |(quantity, color)| {quantity, @@rules[color]} }
      end

      def self.[](color)
        @@rules[color]
      end

      def self.each
        @@rules.each
      end

      def self.each
        @@rules.each do |color, rule|
          yield color, rule
        end
      end

      def self.parse(io : IO)
        text = io.gets(chomp: true)
        return nil if text.nil?

        bag, rules = text.split(" contain ")
        color = bag[0..-6]
        containing = Array({Int32, String}).new

        unless rules == "no other bags."
          rules.split(", ").each do |rule|
            rule_parts = rule.split(' ')
            containing << {rule_parts.first.to_i, rule_parts[1..-2].join(' ')}
          end
        end

        new(color, containing).tap { |rule| @@rules[color] = rule }
      end

      def self.parse_all(io : IO)
        loop do
          rule = Rule.parse(io)
          break if rule.nil?
        end
      end
    end
  end
end
