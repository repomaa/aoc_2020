require "../puzzle"
require "./common"

module AoC2020
  module Day3
    class Part2
      include Puzzle
      include Common

      SLOPES = [
        {1, 1},
        {3, 1},
        {5, 1},
        {7, 1},
        {1, 2},
      ]

      protected def solve
        map = Map.parse(input)

        result = SLOPES.reduce(1) do |acc, slope|
          acc * tree_count_for_slope(map, *slope)
        end

        output.puts(result)
      end
    end
  end
end
