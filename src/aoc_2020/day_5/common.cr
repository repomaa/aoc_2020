module AoC2020::Day5
  module Common
    struct Seat
      getter id : Int32

      @@seats = Hash({Int32, Int32}, self).new
      @@seats_by_id = Hash(Int32, self).new

      def initialize(@row : Int32, @column : Int32)
        @id = row * 8 + column
      end

      def self.taken?(row, column)
        @@seats.has_key?({row, column})
      end

      def self.taken?(id)
        @@seats_by_id.has_key?(id)
      end

      def self.parse(code : String)
        row = binary_search(code[0, 7], 0..127, 'F')
        column = binary_search(code[7, 3], 0..7, 'L')
        new(row, column).tap do |seat|
          @@seats[{row, column}] = seat
          @@seats_by_id[seat.id] = seat
        end
      end

      private def self.binary_search(code, range, first_half_indicator)
        range_end = range.exclusive? ? range.end - 1 : range.end

        result, _ = code.chars.reduce({range.begin, range_end}) do |acc, part|
          middle = acc[1] - (acc[1] - acc[0]) // 2 - 1
          part == first_half_indicator ? {acc[0], middle} : {middle + 1, acc[1]}
        end

        result
      end
    end
  end
end
