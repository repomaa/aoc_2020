require "../puzzle"
require "./common"

module AoC2020::Day5
  class Part1
    include Puzzle
    include Common

    protected def solve
      result = input.each_line.map { |line| Seat.parse(line) }.max_of(&.id)
      output.puts(result)
    end
  end
end
