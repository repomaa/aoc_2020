module AoC2020::Day6
  module Common
    struct Answers
      def initialize(@answers : Array(Set(Char)))
      end

      def yes_count
        count_yesses do |acc, answers|
          acc | answers
        end
      end

      def unanimous_yes_count
        count_yesses do |acc, answers|
          acc & answers
        end
      end

      private def count_yesses
        yesses = @answers.reduce do |acc, answers|
          yield acc, answers
        end

        yesses.size
      end

      def self.parse(io : IO)
        group = io.gets("\n\n", chomp: true)
        return nil if group.nil?

        answers = group.each_line(chomp: true).map do |line|
          Set(Char).new(line.each_char)
        end.to_a

        new(answers.to_a)
      end
    end
  end
end
