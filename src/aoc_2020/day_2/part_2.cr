require "../puzzle"
require "./common"

module AoC2020::Day2
  class Part2
    include Puzzle
    include Common

    private def valid?(entry : Entry) : Bool
      chars = entry.password.chars
      a = chars[entry.limit.begin - 1]?
      b = chars[entry.limit.end - 1]?

      if a == entry.letter
        b != entry.letter
      else
        b == entry.letter
      end
    end
  end
end
