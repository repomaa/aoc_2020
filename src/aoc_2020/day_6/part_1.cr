require "../puzzle"
require "./common"

module AoC2020::Day6
  class Part1
    include Puzzle
    include Common

    protected def solve
      sum = 0

      loop do
        answers = Answers.parse(input)
        break if answers.nil?

        sum += answers.yes_count
      end

      output.puts(sum)
    end
  end
end
