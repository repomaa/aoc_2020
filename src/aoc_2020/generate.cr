module AoC2020
  module Generate
    day = Time.local.day
    dir = File.join(__DIR__, "day_#{day}")
    raise("#{dir} already exists, aborting") if Dir.exists?(dir)

    Dir.mkdir_p(dir)
    File.open(File.join(dir, "common.cr"), "w+") do |file|
      file.puts <<-EOF
      module AoC2020::Day#{day}
        module Common
        end
      end
      EOF
    end

    [1, 2].each do |i|
      File.open(File.join(dir, "part_#{i}.cr"), "w+") do |file|
        file.puts <<-EOF
        require "../puzzle"
        require "./common"

        module AoC2020::Day#{day}
          class Part#{i}
            include Puzzle
            include Common

            protected def solve
              output.puts("NYI")
            end
          end
        end
        EOF
      end
    end

    File.open(File.join(__DIR__, "solutions.cr"), "a+") do |file|
      file.puts(%|require "./day_#{day}/*"|)
    end
  end
end
