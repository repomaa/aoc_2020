module AoC2020::Day1
  module Common
    protected def solve(size)
      entries = input.each_line.map(&.to_i).to_a
      sum = entries.each_combination(size).find(&.sum.== 2020)

      raise "No combination of size #{size} adding up to 2020 found" if sum.nil?

      sum.reduce(1) { |acc, number| acc * number }
    end

    protected def better_solve(size)
      entries = input.each_line.map(&.to_i).to_a.sort
      summands = summands_for(size, 2020, entries)
      raise "no solution found" if summands.empty?

      summands.reduce(1) { |acc, number| acc * number }
    end

    protected def summands_for(size, sum, entries)
      return [] of Int32 if entries.empty?

      if size == 1
        match = entries.bsearch(&.>= sum)
        return match.try(&.== sum) ? [sum] : [] of Int32
      end

      entries.each_with_index do |x, i|
        summands = summands_for(size - 1, sum - x, entries.skip(i))
        return [x] + summands if summands.any?
      end

      [] of Int32
    end
  end
end
