require "../puzzle"
require "./common"

module AoC2020::Day6
  class Part2
    include Puzzle
    include Common

    protected def solve
      sum = 0

      loop do
        answers = Answers.parse(input)
        break if answers.nil?

        sum += answers.unanimous_yes_count
      end

      output.puts(sum)
    end
  end
end
