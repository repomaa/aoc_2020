# AoC 2020

Advent of Code solutions for 2020

## Usage

- Build binaries by running `shards build --release`
- Export the env var `AOC_SESSION_KEY` to retrieve puzzle inputs
- Run `bin/solutions` to show results and benchmarks of solutions
- Run `bin/generate` to generate sources for the current day's puzzle

## Contributors

- [Joakim Repomaa](https://gitlab.com/repomaa) - creator and maintainer
