module AoC2020::Day4
  module Common
    struct Passport
      private REQUIRED_FIELDS = %w[byr iyr eyr hgt hcl ecl pid]
      private VALID_EYE_COLORS = %w[amb blu brn gry grn hzl oth]

      private getter info : Hash(String, String)

      private def initialize(@info)
      end

      def self.parse(io : IO) : Passport?
        info = {} of String => String

        data = io.gets("\n\n", chomp: true)
        return nil if data.nil?

        data.split(/\s+/).each do |datapoint|
          next if datapoint.empty?
          key, value = datapoint.split(':')
          info[key] = value
        end

        new(info)
      end

      def has_fields?
        REQUIRED_FIELDS.all? { |field| info.has_key?(field) }
      end

      def valid?
        has_fields? && valid_byr? && valid_iyr? && valid_eyr? && valid_hgt? &&
        valid_hcl? && valid_ecl? && valid_pid?
      end

      private def valid_byr?
        validate_year("byr", 1920..2002)
      end

      private def valid_iyr?
        validate_year("iyr", 2010..2020)
      end

      private def valid_eyr?
        validate_year("eyr", 2020..2030)
      end

      private def valid_hgt?
        match = info["hgt"].match(/^(?<count>\d+)(?<unit>cm|in)$/)
        return false if match.nil?
        count = match["count"].to_i

        if match["unit"] == "cm"
          count.in?(150..193)
        else
          count.in?(59..76)
        end
      end

      private def valid_hcl?
        !!info["hcl"].match(/^#[0-9a-f]{6}$/)
      end

      private def valid_ecl?
        info["ecl"].in?(VALID_EYE_COLORS)
      end

      private def valid_pid?
        !!info["pid"].match(/^\d{9}$/)
      end

      private def validate_year(key, range)
        info[key].to_i.in?(range)
      end
    end

    def each_passport
      loop do
        passport = Passport.parse(input)
        return if passport.nil?
        yield passport
      end
    end
  end
end
