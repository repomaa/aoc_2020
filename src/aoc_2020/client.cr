require "http/client"

module AoC2020
  module Client
    private CLIENT = HTTP::Client.new("adventofcode.com", tls: true).tap do |client|
      client.before_request do |request|
        request.cookies["session"] = ENV["AOC_SESSION_KEY"]
      end
    end

    class CacheWriter
      def initialize(day, @input : IO)
        Dir.mkdir(".cache") unless Dir.exists?(".cache")
        @cache = File.open(".cache/input_day#{day}", "w+")
      end

      def finalize
        @cache.close
      end

      def read(bytes : Bytes)
        @input.read(bytes).tap do |bytes_read|
          @cache.write(bytes[0, bytes_read])
        end
      end

      def close
        @input.close
        @cache.close
      end
    end

    def self.fetch_input(day)
      cached_input = fetch_input_from_cache(day)
      return yield cached_input unless cached_input.nil?

      CLIENT.get("/2020/day/#{day}/input") do |response|
        input = CacheWriter.new(day, response.body_io)
        begin
          yield input
        ensure
          input.close
        end
      end
    rescue ex
      STDERR.puts("Failed to fetch input, reading from stdin")
      yield STDIN
    end

    def self.fetch_input_from_cache(day)
      return nil unless File.exists?(".cache/input_day#{day}")
      File.open(".cache/input_day#{day}")
    end
  end
end
