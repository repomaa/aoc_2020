require "../puzzle"
require "./common"

module AoC2020
  module Day3
    class Part1
      include Puzzle
      include Common

      SLOPE = {3, 1}

      protected def solve
        map = Map.parse(input)

        output.puts(tree_count_for_slope(map, *SLOPE))
      end
    end
  end
end
