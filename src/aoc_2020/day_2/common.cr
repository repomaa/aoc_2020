module AoC2020::Day2
  module Common
    record Entry, limit : Range(Int32, Int32), letter : Char, password : String do
      def self.parse(line)
        counts, policy, password = line.split(' ')
        min, max = counts.split('-').map(&.to_i)
        letter = policy.chars[0]
        new(min..max, letter, password)
      end
    end

    protected def solve
      entries = input.each_line.map { |line| Entry.parse(line) }
      result = entries.count { |entry| valid?(entry) }

      output.puts(result)
    end

    private abstract def valid?(entry : Entry) : Bool
  end
end
