require "../puzzle"
require "./common"

module AoC2020::Day7
  class Part1
    include Puzzle
    include Common

    protected def solve
      Rule.parse_all(input)
      count = Rule.each.count do |(_, rule)|
        rule.can_contain?("shiny gold")
      end

      output.puts(count)
    end
  end
end
