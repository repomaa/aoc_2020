require "../puzzle"
require "./common"

module AoC2020::Day2
  class Part1
    include Puzzle
    include Common

    private def valid?(entry : Entry) : Bool
      count = 0
      entry.password.each_char do |char|
        count += 1 if char == entry.letter
        return false if count > entry.limit.end
      end

      count >= entry.limit.begin
    end
  end
end
