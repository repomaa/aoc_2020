require "../puzzle"
require "./common"

module AoC2020::Day1
  class Part1
    include Puzzle
    include Common

    protected def solve
      output.puts(better_solve(2))
    end
  end
end
